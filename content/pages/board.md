Title: Board Members
Slug: board

The following is a list of the current NBLUG board members.

* **President:** Allan Cecil
* **Vice President:** E. Frank Ball
* **Treasurer:** Matt Dasilva
* **Scribe:** Glenn Kerbein
* **Directors at Large:** Tom Most and John Morio Sakaguchi
